<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

<link rel="stylesheet" type="text/css"
	href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" />

<c:url value="/css/main.css" var="mycss" />
<link href="${mycss}" rel="stylesheet" />

</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">DEMO</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="/welcome">Home</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">


		<h1>URL SHORTCUT 1.0</h1>
		<h2>${error}</h2>
		<form action="/urlconversion" method="POST">
			<div class="form-group row">
				<label for="url" class="col-sm-2 col-form-label">Lien:</label>
				<input type="text" class="form-control" id="url" name="url" />
			</div>
			<div class="form-group row">
				<button type="submit" class="btn btn-primary">Creer un
					racourci</button>
			</div>
		</form>

		<div>
			<c:if test="${not empty lstUrlObject}">
				<hr>
				<h3>LISTES URL DE RACCOURCI EXISTANT</h3>
				<ul>
					<c:forEach var="urlObject" items="${lstUrlObject}">
						<c:choose>
							<c:when test="${urlObject.keyGenerate != indicatorExist}">
								<li><a href="${urlObject.keyGenerate}">${urlObject.shortUrl}</a></li>
							</c:when>
							<c:otherwise>
								<li class="check"><a href="${urlObject.keyGenerate}">${urlObject.shortUrl}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</ul>
			</c:if>
		</div>
		<br> <br>
		<h2>${errorReverse}</h2>
		<hr>
		<form action="/urlreverse" method="POST">
			<div class="form-group row">
				<label for="url" class="col-sm-2 col-form-label">Lien:</label>
				<input type="text" class="form-control" id="url" name="url" />
			</div>
			<div class="form-group row">
				<button type="submit" class="btn btn-primary">renverser</button>
			</div>
		</form>

		<div>
			<c:if test="${not empty originalUrl}">
				<hr>
				<h3>url originale</h3>
				<ul>
					<li><a href="${originalUrl}">${originalUrl}</a></li>
				</ul>
			</c:if>
		</div>

	</div>
	<script type="text/javascript"
		src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>