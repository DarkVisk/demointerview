package com.alexson.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alexson.model.UrlObject;
import com.alexson.model.UrlRequest;
import com.alexson.services.IReposirotyUrlStorage;
import com.alexson.utility.UtilityShortcut;

@Controller
public class UrlController {

	public static String URL_ERROR = "Le lien non valide il faut un url complet avec http://!";
	public static String URL_SHORCUT = "Le lien de racourci est existant!";
	public static String URL_REVERSE = "Le lien de racourci est non existant veuillez saisir un lien valide!";

	@Autowired
	private IReposirotyUrlStorage repository;

	@RequestMapping(value = "/urlconversion", method = RequestMethod.POST)
	public ModelAndView urlConvertionShort(HttpServletRequest httpRequest, @Valid UrlRequest urlRequest, RedirectAttributes redir) {
		ModelAndView model = new ModelAndView();
		if(urlRequest.getUrl().isEmpty()){
			model.setViewName("redirect:/welcome");
			return model;
		}

		String prefix = UtilityShortcut.getOnlyDomain("http://www.shrinkurl.com/"); // on pourrais passer la valeur pour l'utilisateur pour setter un domaine a lui
		String idGenerate = UtilityShortcut.generateKey(10);
		String domainUrl = UtilityShortcut.getOnlyDomain(urlRequest.getUrl());

		UrlValidator urlValidator = new UrlValidator(); // apache validator
		
		if (!urlValidator.isValid(urlRequest.getUrl())){
			redir.addFlashAttribute("error", URL_ERROR);
			model.setViewName("redirect:/welcome");
		}else if(checkIfUrlExistByDomain(domainUrl)){
			redir.addFlashAttribute("error", URL_SHORCUT);
			UrlObject existObject = repository.findByDomainUrl(domainUrl);
			redir.addFlashAttribute("indicatorExist", existObject.getKeyGenerate());
			model.setViewName("redirect:/welcome");
		}else{
			repository.save(new UrlObject(idGenerate, urlRequest.getUrl(),prefix + "/" + idGenerate, domainUrl));
			model.setViewName("redirect:/welcome");
		}

		return model;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public void redirectionTrueUrl(@PathVariable String id, HttpServletResponse resp) throws IOException {

		UrlObject urlTrue = repository.findByKeyGenerate(id);

		if (urlTrue != null) {
			resp.addHeader("Location", urlTrue.getOrginalUrl());
			resp.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
		} else {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

	}
	
	@RequestMapping(value="/urlreverse", method = RequestMethod.POST)
	public ModelAndView urlConversionReverse(HttpServletRequest httpRequest, @Valid UrlRequest urlRequest, RedirectAttributes redir){
		ModelAndView model = new ModelAndView();
		
		if(checkIfUrlExistByShortUrl(urlRequest.getUrl())){
			UrlObject urlObject = repository.findByShortUrl(urlRequest.getUrl());
			redir.addFlashAttribute("originalUrl", urlObject.getOrginalUrl());
			model.setViewName("redirect:/welcome");
		}else{
			redir.addFlashAttribute("errorReverse", URL_REVERSE);
			model.setViewName("redirect:/welcome");
		}
		
		return model;
	}
	
	private boolean checkIfUrlExistByDomain(String url){
		List<UrlObject> lstUrlObject =  repository.findAll();
		for(int i =0; i < lstUrlObject.size(); i++){
			if(lstUrlObject.get(i).getDomainUrl().equals(url)){	
				return true;
			}
		}
		
		return false;
	}
	
	private boolean checkIfUrlExistByShortUrl(String url){
		List<UrlObject> lstUrlObject =  repository.findAll();
		for(int i =0; i < lstUrlObject.size(); i++){
			if(lstUrlObject.get(i).getShortUrl().equals(url)){	
				return true;
			}
		}
		
		return false;
	}

}
