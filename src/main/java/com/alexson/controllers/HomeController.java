package com.alexson.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alexson.model.UrlObject;
import com.alexson.services.IReposirotyUrlStorage;

@Controller
public class HomeController {
	
	@Autowired
	IReposirotyUrlStorage repository;
	
	@RequestMapping("/welcome")
	public ModelAndView firstPage(){
		ModelAndView model = new ModelAndView();
		List<UrlObject> lstUrlObject =  repository.findAll();
		model.addObject("lstUrlObject", lstUrlObject);
		return model;
	}

}
