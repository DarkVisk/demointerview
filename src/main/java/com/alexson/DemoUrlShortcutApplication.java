package com.alexson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.alexson"})
public class DemoUrlShortcutApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoUrlShortcutApplication.class, args);
	}
}
