package com.alexson.services;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.alexson.model.UrlObject;


public interface IReposirotyUrlStorage extends MongoRepository<UrlObject, String>{
	public UrlObject findByKeyGenerate(String keyGenerate);
	public UrlObject findByDomainUrl(String domainUrl);
	public UrlObject findByShortUrl(String shortUrl);
}
