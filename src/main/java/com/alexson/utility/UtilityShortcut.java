package com.alexson.utility;

import org.apache.commons.lang.RandomStringUtils;

public class UtilityShortcut {
	
	public static String HTTP = "http://";
	public static String HTTPS = "https://";
	public static char LAST_SLASH = '/';
	
	public static String getOnlyDomain(String url){
		
		//premiere cas si on a un http://
		
		if(url.substring(0,7).equals(HTTP)){
			url = url.substring(7);
		}
		
		// deuxieme cas si c'est https securiser
		
		if(url.substring(0, 8).equals(HTTPS)){
			url =url.substring(8);
		}
		
		// troisieme cas on enleve le dernier slash si le url en contient 
		if(url.charAt(url.length()-1) == LAST_SLASH){
			url = url.substring(0, url.length() -1);
		}
		
		return url;
	}
	
	
	// utilise librairie de apache common importer dans les dependances maven
	public static String generateKey(int lenght){
		return RandomStringUtils.randomAlphanumeric(lenght);
	}

}
