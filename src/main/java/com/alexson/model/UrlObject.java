package com.alexson.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

public class UrlObject {
	
	@Id
	private String id;
	
	@Field("keyGenerate")
	private String keyGenerate;
	@Field("originalUrl")
	private String orginalUrl;
	@Field("shortUrl")
	private String shortUrl;
	@Field("domainUrl")
	private String domainUrl;
	
	public UrlObject(){}
	
	public UrlObject(String keyGenerate, String originalUrl, String shortUrl, String domainUrl){
		this.keyGenerate = keyGenerate;
		this.orginalUrl = originalUrl;
		this.shortUrl = shortUrl;
		this.domainUrl = domainUrl;
	}

	public String getKeyGenerate() {
		return keyGenerate;
	}

	public void setKeyGenerate(String keyGenerate) {
		this.keyGenerate = keyGenerate;
	}

	public String getOrginalUrl() {
		return orginalUrl;
	}

	public void setOrginalUrl(String orginalUrl) {
		this.orginalUrl = orginalUrl;
	}

	public String getShortUrl() {
		return shortUrl;
	}

	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}

	public String getDomainUrl() {
		return domainUrl;
	}

	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}
	
	

}
