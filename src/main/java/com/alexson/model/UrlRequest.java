package com.alexson.model;

import javax.validation.constraints.NotNull;

// simple pojo pour mapper un object url

public class UrlRequest {
	
	@NotNull
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
